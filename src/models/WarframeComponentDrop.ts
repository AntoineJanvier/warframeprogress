export interface WarframeComponentDrop {
  chance: number;
  location: string;
  rarity: string;
  type: string;
  rarityColor: string;
  relic: boolean;
}
