import { WarframeComponent } from "./WarframeComponent";

export enum WeaponCategory {
  Primary= 'Primary',
  Secondary = 'Secondary',
  Melee = 'Melee'
}

export interface Weapon {
  id: string;
  name: string;
  description: string;
  imageName: string;
  components: WarframeComponent[];
  built: boolean;
  category: WeaponCategory;
  mastered: boolean;
}