import { WarframeComponentDrop } from "./WarframeComponentDrop";
import { DropLocation } from "./DropLocation";

export interface WarframeComponent {
  id: string;
  description: string;
  name: string;
  itemCount: string;
  actualCount: string;
  imageName: string;
  drops: WarframeComponentDrop[];
  isBuildable: boolean;
  blueprint: boolean;
  built: boolean;
  dropLocations: DropLocation[];
}
