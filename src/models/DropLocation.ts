export interface Relic {
  name: string;
  chance: number;
  upgrade: string;
}

export interface Mission {
  name: string;
  chance: number;
}

export interface DropLocation {
  planet?: string;
  missions?: Mission[];
  era ?: string;
  relics?: Relic[];
}
