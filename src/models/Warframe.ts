import { WarframeComponent } from "./WarframeComponent";

export enum WarframeState {
  NA = 'NA',
  building = 'building',
  built = 'built',
  mastered = 'mastered'
}

export interface Warframe {
  id: string;
  name: string;
  description: string;
  type: 'Warframe';
  imageName: string;
  components: WarframeComponent[];
  productCategory: 'Suits' | 'MechSuits';
  state: WarframeState;
  progress: number;
}