import { Component, OnInit } from '@angular/core';
import { WarframesService } from "../../services/warframes.service";
import { Warframe, WarframeState } from "../../models/Warframe";
import { FormControl } from "@angular/forms";
import { WarframeComponent } from "../../models/WarframeComponent";

@Component({
  selector: 'app-home',
  templateUrl: './warframes.component.html'
})
export class WarframesComponent implements OnInit {

  warframes: Warframe[] = []
  name = new FormControl('')

  constructor(private warframeService: WarframesService) {
  }

  ngOnInit(): void {
    this.getWarframes()
  }

  getWarframes(): void {
    this.warframeService.getWarframes().subscribe({
      next: (data) => {
        console.log(data);
        data.forEach((d) => {
          if (d.id === 'NezhaPrime') {
            console.log(d);
          }
        })
        this.warframes = data;
      },
      error: err => {
        console.error(err)
      }
    })
  }

  getBp(id: string) {
    this.warframeService.getBp(id);
  }

  building(id: string) {
    this.warframeService.building(id);
  }

  build(id: string) {
    this.warframeService.build(id);
  }

  master(id: string) {
    this.warframeService.master(id);
  }

  search(): void {
    let searchName = this.name.value;
    console.log(searchName);
    if (searchName.length) {
      this.warframes = this.warframes.filter(frame => frame.name.toLowerCase().search(searchName.toLowerCase()) > -1)
    } else {
      this.getWarframes()
    }
  }

  isBlueprint(component: WarframeComponent): boolean {
    return ['Blueprint', 'Chassis', 'Neuroptics', 'Systems'].includes(component.name)
  }

  isBlueprintBuildable(component: WarframeComponent): boolean {
    return ['Chassis', 'Neuroptics', 'Systems'].includes(component.name)
  }

  warframeStateDisplay(state: WarframeState): string {
    switch (state) {
      case WarframeState.mastered:
        return 'Mastered';
      case WarframeState.built:
        return 'Built';
      case WarframeState.building:
        return 'Building...';
      case WarframeState.NA:
        return ''
    }
  }

  warframeStateStep(state: WarframeState): number {
    switch (state) {
      case WarframeState.mastered:
        return 3;
      case WarframeState.built:
        return 2;
      case WarframeState.building:
        return 1;
      default:
        return 0
    }
  }

  private warframeStateFromNumber(nb: number): WarframeState {
    switch (nb) {
      case 3:
        return WarframeState.mastered
      case 2:
        return WarframeState.built
      case 1:
        return WarframeState.building
      default:
        return WarframeState.NA
    }
  }

  updateWarframeState(id: string, event: any): void {
    this.warframeService.updateWarframeStatus(id, this.warframeStateFromNumber(parseInt(event.target.value)))
  }

  get totalWarframes(): number {
    return this.warframeService.warframes.length
  }

  get warframesMastered(): number {
    return this.warframeService.warframes.filter((frame) => frame.state === WarframeState.mastered).length
  }

}
