import { Component, OnInit } from '@angular/core';
import { WeaponsService } from "../../services/weapons.service";
import { Weapon, WeaponCategory } from "../../models/Weapon";

@Component({
  selector: 'app-weapons',
  templateUrl: './weapons.component.html'
})
export class WeaponsComponent implements OnInit {

  private allWeapons: Weapon[] = []
  weapons: Weapon[] = []
  selected: WeaponCategory = WeaponCategory.Primary;

  constructor(private weaponService: WeaponsService) {
  }

  ngOnInit(): void {
    this.getWeapons(WeaponCategory.Primary);
  }

  selectPrimary(): void {
    this.filterByType(WeaponCategory.Primary)
  }

  selectSecondary(): void {
    this.filterByType(WeaponCategory.Secondary)
  }

  selectMelee(): void {
    this.filterByType(WeaponCategory.Melee)
  }

  filterByType(type: WeaponCategory): void {
    this.selected = type
    this.weapons = this.allWeapons.filter((weapons) => weapons.category === type)
  }

  getWeapons(type: WeaponCategory): void {
    this.weaponService.getWeapons().subscribe({
      next: (weapons) => {
        this.allWeapons = weapons
        this.filterByType(type)
      },
      error: err => {
        console.error(err)
      }
    })
  }

  get totalWeapons(): number {
    return this.allWeapons.length;
  }

  get weaponsMastered(): number {
    return this.weapons.filter((weapon) => weapon.mastered).length;
  }

}
