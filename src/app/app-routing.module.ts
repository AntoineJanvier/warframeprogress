import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WarframesComponent } from "./warframes/warframes.component";
import { WeaponsComponent } from "./weapons/weapons.component";

const routes: Routes = [
  { path: 'warframes', component: WarframesComponent },
  { path: 'weapons', component: WeaponsComponent },
  { path: '',   redirectTo: '/warframes', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }