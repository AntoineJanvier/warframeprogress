import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { WarframesComponent } from './warframes/warframes.component';
import { ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { WeaponsComponent } from './weapons/weapons.component';
import { DropLocationComponent } from './warframes/drop-location/drop-location.component';

@NgModule({
  declarations: [
    AppComponent,
    WarframesComponent,
    WeaponsComponent,
    DropLocationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
