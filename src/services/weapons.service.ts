import { Injectable } from '@angular/core';
import { Warframe } from "../models/Warframe";
import { HttpClient } from "@angular/common/http";
import { Weapon } from "../models/Weapon";
import { map, Observable, of, tap } from "rxjs";
import { WarframeComponentDrop } from "../models/WarframeComponentDrop";
import { WarframeComponent } from "../models/WarframeComponent";

@Injectable({
  providedIn: 'root'
})
export class WeaponsService {

  private warframeApi = 'https://api.warframestat.us'
  private warframeImageApi = 'https://raw.githubusercontent.com/WFCD/warframe-items/master/data/img/'
  weapons: Weapon[] = [];

  constructor(private http: HttpClient) {
  }


  private setComponentDropColor(drops: WarframeComponentDrop[]): void {
    drops.forEach((drop) => {
      switch (drop.rarity) {
        case 'Rare':
          drop.rarityColor = '#D3C774'
          break;
        case 'Uncommon':
          drop.rarityColor = '#9ABDC8'
          break;
        default:
          drop.rarityColor = '#5E514A'
      }
    })
  }

  getDropsOfWarframe(name: string): Observable<WarframeComponentDrop[]> {
    return this.http.get<Warframe[]>(`${this.warframeApi}/weapons/search/${name}`).pipe(map((warframes: Warframe[]) => {
      let drops: WarframeComponentDrop[] = []
      warframes[0].components?.forEach((component: WarframeComponent) => {
        drops.push(...component.drops);
      })
      return drops
    }))
  }

  getWeapons(): Observable<Weapon[]> {
    if (this.weapons.length === 0) {
      return this.http.get<Weapon[]>(`${this.warframeApi}/weapons`).pipe(
        //map((weapons) => weapons.slice(0, 10)),
        tap((data) => console.log('Data received', data)),
        map((weapons: Weapon[]) => {
          weapons.forEach((weapon) => {
            weapon.id = weapon.name.replace(/\s/g, "");
            weapon.imageName = this.warframeImageApi + weapon.imageName;
            weapon.mastered = false;
            weapon.components?.forEach((component) => {
              component.id = `${weapon.id}-${component.name.replace(/\s/g, "")}`;
              component.imageName = this.warframeImageApi + component.imageName;
              component.drops = component.drops.sort((a, b) => {
                if (a.location < b.location) {
                  return -1;
                }
                if (a.location > b.location) {
                  return 1;
                }
                return 0;
              })
              // if (component.drops.length === 0) {
              //   this.getDropsOfWarframe(weapon.name).subscribe({
              //     next: (drops) => {
              //       console.log(weapon.name, drops);
              //     }
              //   })
              // }
              this.setComponentDropColor(component.drops)
            })
          })
          return weapons
        }),
        tap((weapons: Weapon[]) => {
          //console.log(weapons);
          this.weapons = weapons
        })
      )
    } else {
      return of(this.weapons);
    }
  }
}
