import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, Observable, of, tap } from "rxjs";
import { Warframe, WarframeState } from "../models/Warframe";
import { WarframeComponentDrop } from "../models/WarframeComponentDrop";
import { WarframeComponent } from "../models/WarframeComponent";
import { DropLocation } from "../models/DropLocation";

@Injectable({
  providedIn: 'root'
})
export class WarframesService {

  private warframeApi = 'https://api.warframestat.us'
  private warframeImageApi = 'https://raw.githubusercontent.com/WFCD/warframe-items/master/data/img/'
  warframes: Warframe[] = [];

  constructor(private http: HttpClient) {
  }

  getBp(id: string): void {
    let key = `bp-${id}`
    this.save(key)
  }

  building(id: string): void {
    let key = `building-${id}`
    this.save(key)
    const frameName = key.split('-')[1]
    console.log(frameName);
    if (frameName) {
      let f = this.warframes.find((frame) => frame.id === frameName);
      if (f) {
        this.warframeProgress(f)
      }
    }
  }

  updateWarframeStatus(id: string, status: WarframeState): void {
    const keys = [`NA-${id}`, `building-${id}`, `built-${id}`, `mastered-${id}`];
    keys.forEach((key) => {
      const state = key.split('-')[0]
      if (state === status) {
        this.save(key)
      } else {
        if (localStorage.getItem(key) !== null) {
          localStorage.removeItem(key);
          console.log(`Removed ${key} from storage`);
        }
      }
    })
    let f = this.warframes.find((frame) => {
      return frame.id === id
    })
    if (f) {
      f.state = this.getWarframeStatus(id)
    }
  }

  getWarframeStatus(id: string): WarframeState {
    const keys = [`NA-${id}`, `building-${id}`, `built-${id}`, `mastered-${id}`].reverse();
    let s: WarframeState = WarframeState.NA;
    keys.forEach((key) => {
      if (localStorage.getItem(key) !== null) {
        const state = key.split('-')[0];
        if (state === 'mastered') {
          s = WarframeState.mastered
        } else if (state === 'built') {
          s = WarframeState.built
        } else if (state === 'building') {
          s = WarframeState.building
        } else {
          s = WarframeState.NA
        }
      }
    })
    return s;
  }

  build(id: string): void {
    let key = `built-${id}`
    this.save(key)

    const keyToRemove = `building-${id}`;
    if (localStorage.getItem(keyToRemove) !== null) {
      localStorage.removeItem(keyToRemove);
      console.log(`Removed ${keyToRemove} from storage`);
    }

    const frameName = key.split('-')[1]
    console.log(frameName);
    if (frameName) {
      let f = this.warframes.find((frame) => frame.id === frameName);
      if (f) {
        this.warframeProgress(f)
      }
    }
  }

  master(id: string): void {
    let key = `master-${id}`
    this.save(key)
  }

  save(key: string): void {
    if (localStorage.getItem(key) !== null) {
      localStorage.removeItem(key);
      console.log(`Removed ${key} from storage`);
    } else {
      localStorage.setItem(key, 'y')
      console.log(`Add ${key} to storage`);
    }
  }

  private setComponentDropColor(drops: WarframeComponentDrop[]): void {
    drops.forEach((drop) => {
      switch (drop.rarity) {
        case 'Rare':
          drop.rarityColor = '#D3C774'
          break;
        case 'Uncommon':
          drop.rarityColor = '#9ABDC8'
          break;
        default:
          drop.rarityColor = '#5E514A'
      }
    })
  }

  getDropsOfWarframe(name: string): Observable<WarframeComponentDrop[]> {
    return this.http.get<Warframe[]>(`${this.warframeApi}/warframes/search/${name}`).pipe(map((warframes: Warframe[]) => {
      let drops: WarframeComponentDrop[] = []
      warframes[0].components?.forEach((component: WarframeComponent) => {
        drops.push(...component.drops);
      })
      return drops
    }))
  }

  warframeProgress(frame: Warframe): void {
    let buildableItems = 0;
    let builtItems = 0;
    frame.components?.forEach((component) => {
      component.built = localStorage.getItem(`built-${component.id}`) === 'y'
      component.blueprint = localStorage.getItem(`bp-${component.id}`) === 'y'
      if (['Blueprint', 'Chassis', 'Neuroptics', 'Systems'].includes(component.name)) {
        buildableItems += 1;
      }
      if (component.built) {
        builtItems += 1;
      }
    })
    if ([WarframeState.mastered, WarframeState.built].includes(frame.state)) {
      frame.progress = 100;
    } else if (builtItems === 0) {
      frame.progress = 0;
    } else {
      frame.progress = parseInt(((builtItems / buildableItems) * 100).toFixed(0));
    }
  }

  sortDrops(drops: any[]): DropLocation[] {
    const planets = ['Mercury', 'Venus', 'Earth', 'Lua', 'Mars', 'Deimos', 'Phobos', 'Ceres', 'Jupiter', 'Europa', 'Saturn', 'Uranus', 'Neptune', 'Pluto', 'Sedna', 'Eris', 'Kuva Fortress', 'Void', 'Zariman']
    let dropLocations: DropLocation[] = []
    drops.forEach((drop) => {
      const nameSplitted = drop.location.split('/')
      const planet = nameSplitted.length ? nameSplitted[0] : ''
      if (planets.includes(planet)) {
        let dropLocation: DropLocation | undefined = dropLocations.find((x) => x.planet === planet)
        if (dropLocation) {
          if (!dropLocation.missions) {
            dropLocation.missions = []
          }
          dropLocation.missions.push({ name: nameSplitted[1], chance: drop.chance })
        } else {
          dropLocations.push({
            planet,
            missions: [{ name: nameSplitted[1], chance: drop.chance }]
          })
        }
      } else if (drop.location.split(' ').includes('Relic')) {
        const era = drop.location.split(' ')[0];
        let dropLocation: DropLocation | undefined = dropLocations.find((x) => x.era === era)
        if (dropLocation) {
          if (!dropLocation.relics) {
            dropLocation.relics = []
          }
          dropLocation.relics.push({
            name: drop.location.split(' ')[1],
            chance: drop.chance,
            upgrade: drop.location.split(' ')[3]
          })
        } else {
          dropLocations.push({
            era,
            relics: [{ name: drop.location.split(' ')[1], chance: drop.chance, upgrade: drop.location.split(' ')[3] }]
          })
        }
      }
    })
    return dropLocations
  }

  getWarframes(): Observable<Warframe[]> {
    if (this.warframes.length === 0) {
      return this.http.get<Warframe[]>(`${this.warframeApi}/warframes`).pipe(
        tap((data) => console.log('Data received', data)),
        map((warframes) => warframes.filter((frame) => frame.type === 'Warframe' && frame.productCategory === 'Suits')),
        map((warframes) => {
          warframes.forEach((frame) => {
            frame.id = frame.name.replace(/\s/g, "");
            frame.imageName = this.warframeImageApi + frame.imageName;
            frame.components?.forEach((component) => {
              component.id = `${frame.id}-${component.name.replace(/\s/g, "")}`;
              component.imageName = this.warframeImageApi + component.imageName;
              component.isBuildable = ['Blueprint', 'Chassis', 'Neuroptics', 'Systems'].includes(component.name)
              component.dropLocations = this.sortDrops(component.drops);
              component.drops = component.drops.sort((a, b) => {
                if (a.location < b.location) {
                  return -1;
                }
                if (a.location > b.location) {
                  return 1;
                }
                return 0;
              })

              if (component.drops.length === 0) {
                this.getDropsOfWarframe(frame.name).subscribe()
              }
              if ((frame.name === 'Ash' || frame.name === 'Ash Prime') && component.name === 'Chassis') {
                console.log(component.dropLocations);
              }
              this.setComponentDropColor(component.drops)
            })
          })
          return warframes
        }),
        tap((warframes: Warframe[]) => {
          warframes.forEach((frame) => {
            frame.state = this.getWarframeStatus(frame.id)
            this.warframeProgress(frame);
          })
        }),
        tap((warframes: Warframe[]) => this.warframes = warframes)
      )
    } else {
      return of(this.warframes);
    }
  }
}
